package com.example.manager.Sys.Devops.bean;

public class easyPipe {
    private Integer pipeId;

    private String pipeName;

    public Integer getPipeId() {
        return pipeId;
    }

    public void setPipeId(Integer pipeId) {
        this.pipeId = pipeId;
    }

    public String getPipeName() {
        return pipeName;
    }

    public void setPipeName(String pipeName) {
        this.pipeName = pipeName;
    }

    @Override
    public String toString() {
        return "easyPipe{" +
                "pipeId=" + pipeId +
                ", pipeName='" + pipeName + '\'' +
                '}';
    }
}
