package com.example.manager.Sys.Devops.service.impl;

import com.example.manager.Sys.Devops.bean.easyPipe;
import com.example.manager.Sys.Devops.dao.easyPipeDao;
import com.example.manager.Sys.Devops.service.easyPipeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class easyPipeServiceImpl implements easyPipeService {

    private Logger logger = LoggerFactory.getLogger(easyPipeServiceImpl.class);

    @Autowired
    easyPipeDao pipeDao;

    @Override
    public easyPipe selectPipeByName(String pipeName) {
        easyPipe newPipe = pipeDao.selectPipeByName(pipeName);
        if(newPipe == null){
            logger.info("获取简易流水线" + pipeName + "信息失败！");
            return newPipe;
        }else{
            logger.info("新增简易流水线" + pipeName + "失败！");
            return newPipe;
        }
    }

    @Override
    public int addPipeByName(String pipeName) {
        int isAdded = pipeDao.addPipeByName(pipeName);
        if(isAdded > 0){
            logger.info("新增简易流水线成功！");
            return isAdded;
        }else{
            logger.info("新增简易流水线失败！");
            return isAdded;
        }
    }

    @Override
    public List<easyPipe> getPipeList() {
        List<easyPipe> pipeList = pipeDao.getPipeList();
        if(pipeList == null){
            logger.info("获取简易流水线信息失败！");
            return null;
        }else{
            logger.info("获取简易流水线信息成功！流水线列表为：-----------------------------");
            for(easyPipe pipe : pipeList)
                logger.info(pipe.toString());
            logger.info("------------------------------------------------------------");
            return pipeList;
        }
    }
}
