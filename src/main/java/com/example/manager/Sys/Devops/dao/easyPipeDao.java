package com.example.manager.Sys.Devops.dao;

import com.example.manager.Sys.Devops.bean.easyPipe;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface easyPipeDao {

    int addPipeByName(@Param("pipeName") String pipeName);

    easyPipe selectPipeByName(@Param("pipeName") String pipeName);

    List<easyPipe> getPipeList();
}
