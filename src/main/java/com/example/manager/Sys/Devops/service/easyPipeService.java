package com.example.manager.Sys.Devops.service;

import com.example.manager.Sys.Devops.bean.easyPipe;

import java.util.List;

public interface easyPipeService {
    public int addPipeByName(String pipeName);

    easyPipe selectPipeByName(String pipeName);

    List<easyPipe> getPipeList();
}
