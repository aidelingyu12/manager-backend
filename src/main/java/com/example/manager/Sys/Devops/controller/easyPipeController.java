package com.example.manager.Sys.Devops.controller;

import com.example.manager.Sys.Devops.bean.easyPipe;
import com.example.manager.Sys.Devops.service.easyPipeService;
import com.example.manager.util.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = {"/api"})
@CrossOrigin(origins = {"http://localhost:8081","http://192.168.0.20","http://192.168.0.20:81"})
public class easyPipeController {

    private Logger logger = LoggerFactory.getLogger(easyPipeController.class);

    @Autowired
    easyPipeService pipeService;

    @RequestMapping(value = {"/getPipeByName"})
    @ResponseBody
    public Result getPipeByName(String pipeName){
        easyPipe pipe = pipeService.selectPipeByName(pipeName);
        if(pipe == null){
            return Result.putMETA(404,"查无此用户！");
        }else{
            return Result.putMSGAndData(200,"获取用户成功",pipe);
        }
    }

    @RequestMapping(value = {"/createProject/{pipeName}"})
    @ResponseBody
    public Result createPipeByName(@PathVariable String pipeName){
        int isCreated = pipeService.addPipeByName(pipeName);
        if(isCreated > 0){
            return Result.putMETA(200,"创建简易流水线成功！");
        }else{
            return Result.putMETA(404,"创建简易流水线失败！");
        }
    }

    @RequestMapping(value = {"/easyPipeList"})
    @ResponseBody
    public Result getPipeList(){
        List<easyPipe> pipeList = pipeService.getPipeList();
        if(pipeList == null){
            return Result.putMETA(200,"获取流水线信息失败，流水线为空！");
        }else{
            return Result.putMSGAndData(200,"获取简易流水线成功！",pipeList);
        }
    }
}
