package com.example.manager.Sys.Authority.service.impl;



import com.example.manager.Sys.Authority.bean.TestUser;
import com.example.manager.Sys.Authority.dao.TestUserDao;
import com.example.manager.Sys.Authority.service.TestUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class TestUserServiceImpl implements TestUserService {

    @Autowired
    TestUserDao userDao;

    //获取用户列表
    @Override
    public List<TestUser> selectAll() {
        return userDao.selectAll();
    }

    //获取单个用户信息
    @Override
    public TestUser selectById(Integer id) {
        return userDao.selectById(id);
    }
}
