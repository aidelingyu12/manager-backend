package com.example.manager.Sys.Authority.controller;

import com.example.manager.Sys.Authority.bean.SysUser;
import com.example.manager.Sys.Authority.service.SysUserService;
import com.example.manager.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = {"/api"})
public class LoginController {

    @Autowired
    SysUserService userService;

    @RequestMapping(value = {"/login"})
    @ResponseBody
    //通过用户名查询用户是否存在
    public Result getUserByName(@RequestBody SysUser user){
        String username = user.getUserName();
        //用户被冻结
        int isDisabled = userService.isUserDisabled(username);
        if(isDisabled == 1)
            return Result.putMETA(404, "用户被冻结，请联系管理员！");

        SysUser userResult = userService.getUserLoginByName(username);

        //用户名不存在、密码错误和成功
        if(userResult == null) {
            return Result.putMETA(404, "用户名不存在，获取用户信息失败！");
        }else if(userResult != null && !user.getPassword().equals(userResult.getPassword())){
            return Result.putMETA(404,"用户密码错误，获取用户信息失败！");
        }else{
            return Result.putMETA(200,"获取用户信息成功!");
        }
    }
}
