package com.example.manager.Sys.Authority.dao;

import com.example.manager.Sys.Authority.bean.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

//用户相关
public interface SysUserDao {
    //通过用户名查询,登录用
    public SysUser getUserLoginByName(String username);

    //通过用户名查询
    public SysUser getUserByName(String username);

    //获取用户列表
    public List<SysUser> getUserList();

    //新增用户
    public int addUser(SysUser user);

    //编辑用户
    public int editUser(@Param("user") SysUser user,@Param("userId") Integer userId);

    //删除用户
    public int deleteUserById(Integer userId);

    //冻结用户
    public int disableOrEnableUserById(@Param("userId") Integer userId,@Param("state") Integer state);

    //查询用户是否被冻结
    public int getUserState(@Param("username") String username);

    //通过用户名关键词查询用户
    public List<SysUser> getUsersByQuery(@Param("queryInfo") String queryInfo);
}
