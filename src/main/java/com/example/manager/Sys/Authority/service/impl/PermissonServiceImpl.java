package com.example.manager.Sys.Authority.service.impl;

import com.example.manager.Sys.Authority.bean.Permisson;
import com.example.manager.Sys.Authority.dao.PermissonDao;
import com.example.manager.Sys.Authority.service.PermissonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class PermissonServiceImpl implements PermissonService {

    private static Logger logger = LoggerFactory.getLogger(PermissonServiceImpl.class);

    @Autowired
    PermissonDao permissonDao;

    public List<Permisson> getPermissonList(){
        //获取所有权限列表
        List<Permisson> permissonList = permissonDao.getPermissonList();
        //顶级权限列表
        List<Permisson> parentList = new ArrayList<>();
        //二级权限列表
        List<Permisson> chidrenList = new LinkedList<>();
        //三级权限列表
        List<Permisson> lastChildrenList = new LinkedList<>();
        //将不同等级权限列表进行分类，放入不同列表中
        for(Permisson permisson : permissonList){
            if(permisson.getPmLevel() == 0)
                parentList.add(permisson);
            else if(permisson.getPmLevel() == 1){
                chidrenList.add(permisson);
            }
            else
                lastChildrenList.add(permisson);
        }

        //处理二级权限列表，为其添加子级为三级权限
        for(Permisson children : chidrenList){
            Integer childrenId = children.getPmId();
            List<Permisson> tempChildrenList = new ArrayList<>();
            for(Permisson lastChildren : lastChildrenList){
                if(lastChildren.getPmPid() == childrenId){
                    tempChildrenList.add(lastChildren);
                    lastChildrenList.remove(lastChildren);
                }
            }
            if(tempChildrenList.size() == 0)
                tempChildrenList = null;
            children.setChildren(tempChildrenList);
        }
        logger.info("二级权限" + chidrenList.toString());

        //处理父级权限列表，为其添加二级
        for(Permisson parent : parentList){
            Integer parentId = parent.getPmId();
            List<Permisson> tempChildrenList = new ArrayList<>();
            for(Permisson Children : chidrenList){
                if(Children.getPmPid() == parentId){
                    tempChildrenList.add(Children);
                    lastChildrenList.remove(Children);
                }
            }
            if(tempChildrenList.size() == 0)
                tempChildrenList = null;
            parent.setChildren(tempChildrenList);
        }

        return parentList;
    }
}
