package com.example.manager.Sys.Authority.controller;

import com.example.manager.Sys.Authority.bean.Permisson;
import com.example.manager.Sys.Authority.service.PermissonService;
import com.example.manager.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping(value = {"/api"})
@CrossOrigin(origins = "*")
public class PermissonController {
    @Autowired
    PermissonService permissonService;


    @RequestMapping(value = {"/rights/permission"})
    @ResponseBody
    public Result getPermissonList(){
        List<Permisson> permissonList = permissonService.getPermissonList();
        if(permissonList == null){
            return Result.putMETA(404,"左侧权限列表查无数据，获取失败！");
        }else{
            //List<Object> permissonLists = Arrays.asList( permissonList.toArray() );
            return Result.putMSGAndData(200,"获取左侧权限列表成功！",permissonList);
        }
    }
}
