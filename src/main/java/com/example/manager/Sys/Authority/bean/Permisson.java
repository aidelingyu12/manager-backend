package com.example.manager.Sys.Authority.bean;

import java.io.Serializable;
import java.util.List;

public class Permisson implements Serializable {
    Integer pmId;
    String pmName;
    Integer pmPid;
    Integer pmLevel;
    String pmPath;
    List<Permisson> children;

    public Integer getPmId() {
        return pmId;
    }

    public void setPmId(Integer pmId) {
        this.pmId = pmId;
    }

    public String getPmName() {
        return pmName;
    }

    public void setPmName(String pmName) {
        this.pmName = pmName;
    }

    public Integer getPmPid() {
        return pmPid;
    }

    public void setPmPid(Integer pmPid) {
        this.pmPid = pmPid;
    }

    public Integer getPmLevel() {
        return pmLevel;
    }

    public void setPmLevel(Integer pmLevel) {
        this.pmLevel = pmLevel;
    }

    public List<Permisson> getChildren() {
        return children;
    }

    public String getPmPath() {
        return pmPath;
    }

    public void setPmPath(String pmPath) {
        this.pmPath = pmPath;
    }

    public void setChildren(List<Permisson> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Permisson{" +
                "pmId=" + pmId +
                ", pmName='" + pmName + '\'' +
                ", pmPid=" + pmPid +
                ", pmLevel=" + pmLevel +
                ", pmPath='" + pmPath + '\'' +
                ", children=" + children +
                '}';
    }
}
