package com.example.manager.Sys.Authority.service.impl;

import com.example.manager.Sys.Authority.bean.SysUser;
import com.example.manager.Sys.Authority.dao.SysUserDao;
import com.example.manager.Sys.Authority.service.SysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserServiceImpl implements SysUserService {

    private static Logger logger = LoggerFactory.getLogger(SysUserServiceImpl.class);

    private SysUser resultUser;

    @Autowired
    SysUserDao userDao;

    @Override
    //用户登录
    public SysUser getUserLoginByName(String username) {
        resultUser = userDao.getUserLoginByName(username);
        logger.info("登录用户： " + resultUser.toString());
        return userDao.getUserLoginByName(username);
    }

    @Override
    //用户编辑
    public SysUser getUserByName(String username) {
        resultUser = userDao.getUserByName(username);
        logger.info("用户信息： " + resultUser.toString());
        return resultUser;
    }

    @Override
    public List<SysUser> getUserList() {
        List<SysUser> userList = userDao.getUserList();
        return userList;
    }

    @Override
    public int addUser(SysUser user) {
        logger.info("添加的用户信息为： " + user);
        return userDao.addUser(user);
    }

    @Override
    public int editUser(SysUser user,Integer userId) {
        logger.info("编辑后的用户信息为： " + user);
        return userDao.editUser(user,userId);
    }

    @Override
    public int deleteUser(Integer userId) {
        logger.info("需要删除的用户id为： " + userId);
        return userDao.deleteUserById(userId);
    }

    @Override
    public int diableOrEnableUser(Integer userId,Integer state) {
        if(state == 1)
            logger.info("需要禁用的用户id为： " + userId + " ,用户状态变为" + state + " , 冻结");
        else
            logger.info("需要解冻的用户id为： " + userId + " ,用户状态变为" + state + " , 正常");
        return userDao.disableOrEnableUserById(userId,state);
    }

    @Override
    public int isUserDisabled(String username) {
        int isDisabled = userDao.getUserState(username);
        if(isDisabled == 1)
            logger.info("用户被冻结，无法登录！");
        return isDisabled;
    }

    @Override
    public List<SysUser> getUsersByQuery(String query) {
        return userDao.getUsersByQuery(query);
    }
}
