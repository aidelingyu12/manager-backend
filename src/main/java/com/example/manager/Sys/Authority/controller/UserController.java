package com.example.manager.Sys.Authority.controller;

import com.example.manager.Sys.Authority.bean.SysUser;
import com.example.manager.Sys.Authority.service.SysUserService;
import com.example.manager.util.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(value = {"/api"})
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    SysUserService userService;

    int modflag;


    @ResponseBody
    @RequestMapping(value = {"/user"})
    public Result getUserList(){
        List<SysUser> userList = userService.getUserList();
        if(userList == null){
            logger.info("获取用户列表数据失败！");
            return Result.putMETA(404,"获取用户列表数据失败！");
        }else{
            //List<Object> userLists = Arrays.asList( userList.toArray() );
            logger.info("获取用户列表数据成功！");
            return Result.putMSGAndData(200,"获取用户列表数据成功！",userList);
        }
    }

    @ResponseBody
    @RequestMapping(value = {"/addUser"},method = RequestMethod.POST)
    public Result addUser(@RequestBody SysUser user){
        String username = user.getUserName();
        modflag = userService.addUser(user);
        if(modflag > 0) {
            logger.info("新增用户成功！");
            return Result.putMSGAndData(200,"新增用户成功！",userService.getUserByName(username));
        }else{
            logger.info("新增用户失败！");
            return Result.putMETA(404,"新增用户失败！");
        }
    }

    @ResponseBody
    @RequestMapping(value = {"/editUser/{userId}"},method = RequestMethod.PUT)
    public Result editUser(@RequestBody SysUser user,@PathVariable Integer userId){
        String username = user.getUserName();
        if(userService.getUserByName(username) == null) {
            logger.info("用户名不存在，编辑用户失败！");
            return Result.putMETA(404,"用户名不存在，编辑用户失败！");
        }else{
            modflag = userService.editUser(user,userId);
            if(modflag > 0){
                logger.info("编辑用户成功！");
                return Result.putMSGAndData(200,"编辑用户成功！",userService.getUserByName(username));
            }else{
                logger.info("用户名不存在，编辑用户失败！");
                return Result.putMETA(404,"用户名不存在，编辑用户失败！");
            }
        }
    }

    @ResponseBody
    @RequestMapping(value = {"/deleteUser/{userId}"},method = RequestMethod.DELETE)
    public Result deleteUser(@PathVariable Integer userId){
        modflag = userService.deleteUser(userId);
        if(modflag > 0){
            logger.info("删除用户成功！");
            return Result.putMETA(200,"删除用户成功");
        }
        else{
            logger.info("删除用户失败！");
            return Result.putMETA(404,"删除用户失败");
        }
    }

    @ResponseBody
    @RequestMapping(value = {"/disableOrEnableUser/{disableOrEnable}/{userId}"})
    public Result disableOrEnableUser(@PathVariable String disableOrEnable,@PathVariable int userId){
        if(disableOrEnable.equals("disable")){
            modflag = userService.diableOrEnableUser(userId,1);
            if(modflag > 0){
                logger.info("禁用用户" + userId + "成功！");
                return Result.putMETA(200,"禁用用户成功");
            }
            else{
                logger.info("禁用用户" + userId + "失败！");
                return Result.putMETA(404,"禁用用户失败");
            }
        }else {
            modflag = userService.diableOrEnableUser(userId,0);
            if(modflag > 0){
                logger.info("解冻用户" + userId + "成功！");
                return Result.putMETA(200,"解冻用户成功");
            }
            else{
                logger.info("解冻用户" + userId + "失败！");
                return Result.putMETA(404,"解冻用户失败");
            }
        }

    }

    @ResponseBody
    @RequestMapping(value = {"/searchUsers/{queryInfo}"})
    public Result searchUsersByQuery(@PathVariable String queryInfo){
        System.out.println(queryInfo);
        List<SysUser> users = userService.getUsersByQuery(queryInfo);
        if(users == null){
            logger.info("通过用户名参数" + queryInfo + "查询用户失败！");
            return Result.putMETA(404,"查询用户失败");
        }else{
            logger.info("通过用户名参数" + queryInfo + "查询用户成功！");
            return Result.putMSGAndData(200,"查询用户成功",users);
        }
    }

}
