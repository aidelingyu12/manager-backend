package com.example.manager.Sys.Authority.dao;

import com.example.manager.Sys.Authority.bean.TestUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface TestUserDao {

    //获取用户列表
    List<TestUser> selectAll();

    //获取单个用户信息
    TestUser selectById (Integer id);
}
