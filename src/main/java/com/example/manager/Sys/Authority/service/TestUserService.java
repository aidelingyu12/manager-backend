package com.example.manager.Sys.Authority.service;



import com.example.manager.Sys.Authority.bean.TestUser;

import java.util.List;

public interface TestUserService {

    //获取用户列表
    List<TestUser> selectAll();

    //获取单个用户信息
    TestUser selectById(Integer id);
}
