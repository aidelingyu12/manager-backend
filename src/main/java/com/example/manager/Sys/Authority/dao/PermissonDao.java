package com.example.manager.Sys.Authority.dao;

import com.example.manager.Sys.Authority.bean.Permisson;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface PermissonDao {
    List<Permisson> getPermissonList();
}
