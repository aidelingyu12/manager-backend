package com.example.manager.Sys.Authority.controller;

import com.example.manager.Sys.Authority.bean.TestUser;
import com.example.manager.Sys.Authority.service.TestUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TestUserController {

    @Autowired
    private TestUserService userService;

    //获取所有用户信息
    @RequestMapping("/all")
    public void testSelectAll(){
        List<TestUser> userList = userService.selectAll();
        for(TestUser user : userList)
            System.out.println(user.toString());
    }

    //获取单个用户信息
    @RequestMapping("/one")
    public void testSelectOne(){
        TestUser user = userService.selectById(1);
        System.out.println(user.toString());
    }
}
