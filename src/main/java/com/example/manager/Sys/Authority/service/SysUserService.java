package com.example.manager.Sys.Authority.service;

import com.example.manager.Sys.Authority.bean.SysUser;

import java.util.List;

public interface SysUserService {

    public SysUser getUserLoginByName(String username);

    public SysUser getUserByName(String username);

    public List<SysUser> getUserList();

    public int addUser(SysUser user);

    public int editUser(SysUser user,Integer userId);

    public int deleteUser(Integer userId);

    public int diableOrEnableUser(Integer userId,Integer state);

    public int isUserDisabled(String username);

    public List<SysUser> getUsersByQuery(String query);
}
