package com.example.manager.Sys.Topic.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.example.manager.Sys.Topic.cores.utils.RedisUtil;
import com.example.manager.Sys.Topic.dao.topicDao;
import com.example.manager.Sys.Topic.service.TimingCrawlBaiduService;
import com.example.manager.Sys.Topic.testTopic;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
@EnableScheduling
public class TimingCrawlBaiduServiceImpl implements TimingCrawlBaiduService {

    @Autowired
    topicDao topicDao;

    @Resource
    private RedisUtil redisUtil;


    JSONObject jsonObject;

    //private static int ExpireTime = 60;   // redis中存储的过期时间60s

    final Logger logger = LoggerFactory.getLogger(testTopic.class);

    SimpleDateFormat df;

    @Scheduled(cron = "0 */10 * * * ?")
    public List<Object> crawlBaiduHotSearch(){
        logger.info("开始定时爬取数据");
        List<Object> result = getBaiduTopicOnce();
        logger.info("爬取数据结束");
        return result;
    }

    public List<Object> getBaiduTopicOnce(){

        //热搜页面地址
        String url = "http://top.baidu.com/buzz?b=1&fr=topindex";

       try{
           //获取页面
           Document doc = Jsoup.connect(url).get();

           //获取热搜列表
           Elements topicTitle = doc.getElementsByTag("tbody").select("tr");

           //结果集
           JSONObject jsonObject;

           //日期
           SimpleDateFormat df;

           //计数器,用来统计每次插入数据的条数和返回条数
           int counter = 0;

           for(Element topics : topicTitle){
               if(counter == 20)
                   break;
               if(topics.child(0).text().equals("排名"))
                   continue;
               else if(topics.className().equals("item-tr")){
                   continue;
               }else{
                   jsonObject = new JSONObject();

                   //排名
                   jsonObject.put("rank",topics.child(0).text());

                   //标题
                   jsonObject.put("topicTitle",topics.child(1).selectFirst("a").text());
                   //链接
                   jsonObject.put("href",topics.child(1).selectFirst("a").attr("href"));
                   //热度
                   jsonObject.put("hot",topics.child(3).text());

                   //插入时间
                   df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                   String date = df.format(new Date());
                   jsonObject.put("createTime",date);
                   logger.info(jsonObject.toJSONString());
                   redisUtil.lSet("baidu",jsonObject);
                   counter ++;
               }
           }
           Long baiduTopicSize = redisUtil.lGetListSize("baidu");
           if(baiduTopicSize <= 0){
               jsonObject = null;
               return null;
           }else{
               jsonObject = null;
               List<Object> resultList = redisUtil.lGet("baidu",baiduTopicSize-counter,baiduTopicSize-1);
               return resultList;
           }
       }catch (Exception e){
            e.printStackTrace();
            //return null;
            return null;
        }
    }
}
