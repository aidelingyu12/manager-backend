package com.example.manager.Sys.Topic.controller;

import com.example.manager.Sys.Topic.cores.utils.topicResult;
import com.example.manager.Sys.Topic.service.zhihuTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = {"/api"})
@CrossOrigin(origins = "*")
public class zhihuTopicController {


    @Autowired
    private zhihuTopicService zhihuTopicService;

    @RequestMapping(value = {"/zhihuTopics"})
    @ResponseBody
    public topicResult getZhihuTopics(){

        return zhihuTopicService.getZhihuTopics();
    }
}
