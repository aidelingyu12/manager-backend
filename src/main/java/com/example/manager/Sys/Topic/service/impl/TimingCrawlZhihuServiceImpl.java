package com.example.manager.Sys.Topic.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.manager.Sys.Topic.cores.utils.RedisUtil;
import com.example.manager.Sys.Topic.service.TimingCrawlZhihuService;
import com.example.manager.Sys.Topic.testTopic;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@EnableScheduling
@Configuration
public class TimingCrawlZhihuServiceImpl implements TimingCrawlZhihuService {

    //chromeDriver路径
    @Value("${chrome-driver.path}")
    private String ChromeDriverPath;

    @Resource
    private RedisUtil redisUtil;


    JSONObject jsonObject;


    //private static int ExpireTime = 60;   // redis中存储的过期时间60s

    final Logger logger = LoggerFactory.getLogger(testTopic.class);

    SimpleDateFormat df;

    @Scheduled(cron = "0 */8 * * * ?")
    public List<Object> crawlZhihuHotSearch() {
        logger.info("开始定时爬取数据");
        List<Object> result = getZhihuTopicOnce();
        logger.info("爬取数据结束");
        return result;
    }


    public List<Object> getZhihuTopicOnce() {

        //知乎热搜网站
        String url = "https://www.zhihu.com/billboard";

        jsonObject = new JSONObject();

        try {
            //计数器,用来统计每次插入数据的条数和返回条数
            int counter = 0;
            //加载chromedriver
            System.setProperty("webdriver.chrome.driver", ChromeDriverPath);
            //System.setProperty("webdriver.chrome.driver", "F:\\driver\\chromedriver.exe");
            //设置chrome选项
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless");
            options.addArguments("--disable-gpu");
            options.addArguments("--no-sandbox");
            //建立selenium 驱动
            ChromeDriver browser = new ChromeDriver(options);
            browser.get(url);
            //等待加载页面
            browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            //获取热搜列表，热搜列表都是a标签
            //a标签中有三个div,第一个为rank，第二个为title和hot，href为单击响应事件，base64编码值被放在a标签的data-za-extra-module属性中
            List<WebElement> hotList = browser.findElementsByClassName("HotList-item");

            //遍历，寻找需要的元素
            for (WebElement element : hotList) {
                if(counter == 40)
                    break;
                //获取rank
                String rank = element.findElement(By.className("HotList-itemPre")).findElement(By.className("HotList-itemIndex")).getText();

                //获取title和热度
                WebElement titleAndHot = element.findElement(By.className("HotList-itemBody"));
                //获取title
                String topicTitle = titleAndHot.findElement(By.className("HotList-itemTitle")).getText();
                //获取hot
                String hot = titleAndHot.findElement(By.className("HotList-itemMetrics")).getText();

                //现在开始获取href；超链接不直接展示，需要通过click事件来获取或者通过页面编码来获取id；下面通过编码来获取
                //获取href的id号编码，编码格式为key:value
                String s = element.getAttribute("data-za-extra-module");
                //将其转为json
                JSON j = JSON.parseObject(s);
                //通过json获取其value，所有热搜href编码的key都是相同的
                JSONObject baseObj64 = (JSONObject) j;
                String base64 = baseObj64.getString("attached_info_bytes");
                //解码，编码格式为base64，将其存入byte数组
                byte[] base = Base64.getDecoder().decode(base64);
                //将编码转换为string
                String baseOrigin = new String(base);
                //分割编码，存入数组
                String[] topicIdNum = baseOrigin.split("\t");
                String topicToBeUse = null;
                //从编码数组中获取href的id
                for (int i = 0; i < topicIdNum.length; i++) {
                    //每一个编码中都有billboard关键字，billboard关键字前面就是id
                    if (topicIdNum[i].contains("billboard")) {
                        //获取id
                        topicToBeUse = topicIdNum[i - 1];
                        //每个id的长度固定为13，是上面裁剪后数值的后13位，所以需要裁剪
                        String cut = topicToBeUse.substring(topicToBeUse.length() - 13);
                        //去掉空格，有些13位id包含空格
                        String topicId = cut.split("x")[0].trim();
                        //拼接
                        String topicHref = "https://www.zhihu.com/question/" + topicId + "?utm_division=hot_list_page";
                        //链接
                        jsonObject.put("href", topicHref);
                    }
                }

                //下面开始获取rank；因为rank排列为01,02,03...所以需要剪切为1,2,3...格式
                if(rank.charAt(0) == '0'){
                    //获取第二位
                    Character rankSingle = rank.charAt(1);
                    //热度
                    jsonObject.put("rank", rankSingle.toString());
                }else{
                    //热度
                    jsonObject.put("rank", rank);
                }

                //标题
                jsonObject.put("topicTitle", topicTitle);
                //热度
                jsonObject.put("hot", hot.split(" ")[0]);
                df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = df.format(new Date());
                //时间
                jsonObject.put("createTime", date);
                redisUtil.lSet("zhihu",jsonObject);
                counter ++;
                logger.info(jsonObject.toJSONString());
            }

            browser.quit();

            Long zhihuTopicSize = redisUtil.lGetListSize("zhihu");

            if (zhihuTopicSize <= 0) {
                jsonObject = null;
                return null;
            } else {
                jsonObject = null;
                List<Object> resultList = redisUtil.lGet("zhihu", zhihuTopicSize - counter, zhihuTopicSize - 1);
                return resultList;
            }
    }catch(Exception e){
            e.printStackTrace();

        }
        return null;
    }

}
