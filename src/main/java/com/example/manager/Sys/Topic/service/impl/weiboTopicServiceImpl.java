package com.example.manager.Sys.Topic.service.impl;

import com.example.manager.Sys.Topic.cores.utils.RedisUtil;
import com.example.manager.Sys.Topic.cores.utils.topicResult;
import com.example.manager.Sys.Topic.dao.topicDao;
import com.example.manager.Sys.Topic.service.TimingCrawlBaiduService;
import com.example.manager.Sys.Topic.service.TimingCrawlWeiboService;
import com.example.manager.Sys.Topic.service.baiduTopicService;
import com.example.manager.Sys.Topic.service.weiboTopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class weiboTopicServiceImpl implements weiboTopicService {

    private Logger logger = LoggerFactory.getLogger(weiboTopicServiceImpl.class);

    @Autowired
    topicDao topicDao;

    @Autowired
    TimingCrawlWeiboService timingCrawlService;

    @Resource
    private RedisUtil redisUtil;



    @Override
    public topicResult getWeiboTopics()   {
        if(redisUtil.lGetListSize("weibo") <= 0){
            logger.info("开始首次获取数据");
            List<Object> topicJsonResult = timingCrawlService.getWeiboTopicOnce();
            return topicResult.putMSGAndData(200,"获取微博热搜成功！",topicJsonResult);
        }else{
            Long weiboTopicSize = redisUtil.lGetListSize("weibo");
            logger.info("开始获取微博热搜数据");
            List<Object> resultList = redisUtil.lGet("weibo",weiboTopicSize-40,weiboTopicSize-1);
            if(resultList != null){
                for(Object obj : resultList)
                    logger.info(obj.toString());
                return topicResult.putMSGAndData(200,"获取微博热搜成功！",resultList);
            }else{
                return null;
            }
        }
    }

}
