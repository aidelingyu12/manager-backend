package com.example.manager.Sys.Topic.service;

import java.util.List;


public interface TimingCrawlZhihuService {
    List<Object> crawlZhihuHotSearch();

    List<Object> getZhihuTopicOnce();
}
