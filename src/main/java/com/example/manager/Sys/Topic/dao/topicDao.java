package com.example.manager.Sys.Topic.dao;

import com.example.manager.Sys.Topic.bean.Topic;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface topicDao {

    List<Topic> getAllTopics();

    void setTopicInfo(List<String> topicInfo);

    int insertBaiduTopics(@Param("topicList") List<Object> topicList);

    //int totalTopics();
}
