package com.example.manager.Sys.Topic.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.example.manager.Sys.Topic.cores.utils.RedisUtil;
import com.example.manager.Sys.Topic.dao.topicDao;
import com.example.manager.Sys.Topic.service.TimingCrawlWeiboService;
import com.example.manager.Sys.Topic.testTopic;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@EnableScheduling
public class TimingCrawlWeiboServiceImpl implements TimingCrawlWeiboService {

    @Autowired
    topicDao topicDao;

    @Resource
    private RedisUtil redisUtil;


    JSONObject jsonObject;

    //private static int ExpireTime = 60;   // redis中存储的过期时间60s

    final Logger logger = LoggerFactory.getLogger(testTopic.class);

    SimpleDateFormat df;

    @Scheduled(cron = "0 */13 * * * ?")
    public List<Object> crawlWeiboHotSearch(){
        logger.info("开始定时爬取数据");
        List<Object> result = getWeiboTopicOnce();
        logger.info("爬取数据结束");
        return result;
    }

    public List<Object> getWeiboTopicOnce(){

        jsonObject = new JSONObject();

            String url = "https://s.weibo.com/top/summary?cate=realtimehot";

            try{
                //获取页面
                Document doc = Jsoup.connect(url).get();
                //获取热搜列表
                Elements tr = doc.getElementsByTag("tbody").select("tr");

                //计数器,用来统计每次插入数据的条数和返回条数
                int counter = 0;

                logger.info("开始插入微博热搜数据： -------------------");
                for(Element per: tr){
                    if(counter == 40)
                        break;
                    if(per.child(0).text().equals(""))
                        continue;
                    Elements e = per.child(1).select("a");
                    Elements hot = per.child(1).select("span");
                    jsonObject.put("rank",per.child(0).text());
                    jsonObject.put("topicTitle",e.text());
                    jsonObject.put("hot",hot.text());
                    if(e.hasAttr("href_to"))
                        jsonObject.put("href","https://s.weibo.com/" + e.attr("href_to"));
                    else
                        jsonObject.put("href","https://s.weibo.com/" + e.attr("href"));

                    //插入时间
                    df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String date = df.format(new Date());
                    jsonObject.put("createTime",date);
                    //logger.info(jsonObject.toJSONString());
                    redisUtil.lSet("weibo",jsonObject);
                    counter ++;
                }
                //logger.info(jsonObject.toJSONString());
                Long weiboTopicSize = redisUtil.lGetListSize("weibo");
                if(weiboTopicSize <= 0){
                    jsonObject = null;
                    return null;
                }else{
                    jsonObject = null;
                    List<Object> resultList = redisUtil.lGet("weibo",weiboTopicSize-counter,weiboTopicSize-1);
                    for(Object o : resultList)
                        logger.info(o.toString());
                    return resultList;
                }
            }catch(Exception e){
                return null;
            }

    }


}
