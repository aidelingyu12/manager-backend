package com.example.manager.Sys.Topic.controller;

import com.example.manager.Sys.Topic.cores.utils.topicResult;
import com.example.manager.Sys.Topic.service.weiboTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = {"/api"})
@CrossOrigin(origins = "*")
public class weiboTopicController {


    @Autowired
    private weiboTopicService weiboTopicService;

    @RequestMapping(value = {"/weiboTopics"})
    @ResponseBody
    public topicResult getWeiboTopics(){

        return weiboTopicService.getWeiboTopics();
    }
}
