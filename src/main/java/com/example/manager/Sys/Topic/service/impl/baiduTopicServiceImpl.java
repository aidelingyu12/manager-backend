package com.example.manager.Sys.Topic.service.impl;

import com.example.manager.Sys.Topic.cores.utils.RedisUtil;
import com.example.manager.Sys.Topic.cores.utils.topicResult;
import com.example.manager.Sys.Topic.dao.topicDao;
import com.example.manager.Sys.Topic.service.TimingCrawlBaiduService;
import com.example.manager.Sys.Topic.service.baiduTopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class baiduTopicServiceImpl implements baiduTopicService {

    private Logger logger = LoggerFactory.getLogger(baiduTopicServiceImpl.class);

    @Autowired
    topicDao topicDao;

    @Autowired
    TimingCrawlBaiduService timingCrawlService;

    @Resource
    private RedisUtil redisUtil;


    @Override
    public int addBaiduTopics(List<Object> topicList) {
        int isInsert = topicDao.insertBaiduTopics(topicList);
        return isInsert;
    }





    @Override
    public topicResult getBaiduTopics()   {
        if(redisUtil.lGetListSize("baidu") <= 0){
            logger.info("开始首次获取数据");
            List<Object> topicJsonResult = timingCrawlService.getBaiduTopicOnce();
            return topicResult.putMSGAndData(200,"获取百度热搜成功！",topicJsonResult);
        }else{
            Long baiduTopicSize = redisUtil.lGetListSize("baidu");
            logger.info("开始获取百度热搜数据");
            List<Object> resultList = redisUtil.lGet("baidu",baiduTopicSize-20,baiduTopicSize-1);
            if(resultList != null){
                for(Object obj : resultList)
                    logger.info(obj.toString());
                return topicResult.putMSGAndData(200,"获取百度热搜成功！",resultList);
            }else{
                return null;
            }
        }
    }

}
