package com.example.manager.Sys.Topic.service;

import java.util.List;


public interface TimingCrawlBaiduService {
    List<Object> crawlBaiduHotSearch();

    List<Object> getBaiduTopicOnce();
}
