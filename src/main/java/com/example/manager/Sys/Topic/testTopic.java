package com.example.manager.Sys.Topic;

import com.alibaba.fastjson.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

//使用jsoup爬取百度热搜
public class testTopic {

    public void getResult() throws IOException {

        //热搜页面地址
        String url = "http://top.baidu.com/buzz?b=1&fr=topindex";

        //获取页面
        Document doc = Jsoup.connect(url).get();

        //获取热搜列表
        Elements topicTitle = doc.getElementsByTag("tbody").select("tr");

        //结果集
        JSONObject jsonObject;

        //日期
        SimpleDateFormat df;

        for(Element topics : topicTitle){
            if(topics.child(0).text().equals("排名"))
                continue;
            else if(topics.className().equals("item-tr")){
                continue;
            }else{
                jsonObject = new JSONObject();

                //排名
                jsonObject.put("rank",topics.child(0).text());

                //标题
                jsonObject.put("topicTitle",topics.child(1).text());
                //链接
                jsonObject.put("href",topics.child(1).selectFirst("a").attr("href"));
                //热度
                jsonObject.put("hot",topics.child(3).text());

                //插入时间
                df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = df.format(new Date());
                jsonObject.put("createTime",date);

                System.out.println(jsonObject.toJSONString());
            }
        }

    }

    public static void main(String[] args) throws IOException {
        new testTopic().getResult();
    }

}
