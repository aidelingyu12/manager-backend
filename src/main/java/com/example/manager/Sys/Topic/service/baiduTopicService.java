package com.example.manager.Sys.Topic.service;

import com.example.manager.Sys.Topic.bean.Topic;
import com.example.manager.Sys.Topic.cores.utils.topicResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface baiduTopicService {
    topicResult getBaiduTopics();

    int addBaiduTopics(List<Object> topicList);
}

