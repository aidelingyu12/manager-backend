package com.example.manager.Sys.Topic.bean;

import java.util.Date;
import java.util.List;

public class Topic {
    private Integer topicId;
    private Date createTime;
    private Date deleteTime;
    private String baseUrl;
    private Integer isDeleted;
    private String topicTitle;
    private Date lastUpdateTime;
    private String topicTitleCh;
    private List<Object> topicInfo;

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setLastUpdateTime(Date lastUpdateIime) {
        this.lastUpdateTime = lastUpdateIime;
    }

    public List<Object> getTopicInfo() {
        return topicInfo;
    }

    public void setTopicInfo(List<Object> topicInfo) {
        this.topicInfo = topicInfo;
    }


    public String getTopicTitleCh() {
        return topicTitleCh;
    }

    public void setTopicTitleCh(String topicTitleCh) {
        this.topicTitleCh = topicTitleCh;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "topicId=" + topicId +
                ", createTime=" + createTime +
                ", deleteTime=" + deleteTime +
                ", baseUrl='" + baseUrl + '\'' +
                ", isDeleted=" + isDeleted +
                ", topicTitle='" + topicTitle + '\'' +
                ", lastUpdateTime=" + lastUpdateTime +
                ", topicTitleCh='" + topicTitleCh + '\'' +
                ", topicInfo=" + topicInfo +
                '}';
    }
}
