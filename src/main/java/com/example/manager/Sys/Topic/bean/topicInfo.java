package com.example.manager.Sys.Topic.bean;

import java.util.Date;

public class topicInfo {
    String rank;
    Date createTime;
    String href;
    String hot;
    String topicTitle;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getHot() {
        return hot;
    }

    public void setHot(String hot) {
        this.hot = hot;
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    @Override
    public String toString() {
        return "topicInfo{" +
                "rank='" + rank + '\'' +
                ", createTime=" + createTime +
                ", href='" + href + '\'' +
                ", hot='" + hot + '\'' +
                ", topicTitle='" + topicTitle + '\'' +
                '}';
    }
}
