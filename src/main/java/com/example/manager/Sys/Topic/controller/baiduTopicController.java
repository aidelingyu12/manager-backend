package com.example.manager.Sys.Topic.controller;

import com.example.manager.Sys.Topic.cores.utils.topicResult;
import com.example.manager.Sys.Topic.service.baiduTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = {"/api"})
@CrossOrigin(origins = "*")
public class baiduTopicController {


    @Autowired
    private baiduTopicService topicService;

    @RequestMapping(value = {"/baiduTopics"})
    @ResponseBody
    public topicResult getBaiduTopics(){

        return topicService.getBaiduTopics();
    }
}
