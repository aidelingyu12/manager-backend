package com.example.manager.Sys.Topic.cores.utils;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.example.manager.Sys.Topic.bean.Topic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class topicResult extends HashMap {
    private int hot;
    private String title;
    private String url;

    public topicResult(){}

    public topicResult(int hot,String title,String url){
        this.hot = hot;
        this.title = title;
        this.url = url;
    }

    public static topicResult putMSGAndData (int code, String msg,List<Object> topicList){
        topicResult META = new topicResult();
        topicResult result = new topicResult();
        META.put("code",code);
        META.put("msg",msg);
        result.put("data",topicList);
        result.put("meta",META);
        return result;
    }

    public static topicResult putMSGAndDataForTopics (int code, String msg,List<Topic> topicList){
        topicResult META = new topicResult();
        topicResult result = new topicResult();
        META.put("code",code);
        META.put("msg",msg);
        result.put("data",topicList);
        result.put("meta",META);
        return result;
    }

}
