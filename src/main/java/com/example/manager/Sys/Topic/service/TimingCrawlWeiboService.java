package com.example.manager.Sys.Topic.service;

import java.util.List;


public interface TimingCrawlWeiboService {
    List<Object> crawlWeiboHotSearch();

    List<Object> getWeiboTopicOnce();
}
