package com.example.manager.Sys.Topic.controller;

import com.example.manager.Sys.Topic.bean.Topic;
import com.example.manager.Sys.Topic.cores.utils.RedisUtil;
import com.example.manager.Sys.Topic.cores.utils.topicResult;
import com.example.manager.Sys.Topic.service.getTopicService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

@Controller
@RequestMapping(value = {"/api"})
@CrossOrigin(origins = "*")
public class getTopicController {

    @Resource
    private RedisUtil redisUtil;


    @Autowired
    getTopicService topicService;

    final Logger logger = LoggerFactory.getLogger(getTopicController.class);



    @ResponseBody
    @RequestMapping(value = {"/getAllTopics"})
    public topicResult getAllTopics(){

        //所有热搜列表
        List<String> infoList = new LinkedList<>();

        //获取所有的热搜类型
        List<Topic> topics = topicService.getAllTopics();

        logger.info("热搜列表数据已获取");
        for(Topic top : topics)
            logger.info(top.toString());
        logger.info("热搜列表数据获取完毕");
        if(topics != null){
            return topicResult.putMSGAndDataForTopics(200,"获取所有热搜列表成功",topics);
        }else{
            return topicResult.putMSGAndDataForTopics(404,"获取所有热搜列表失败",null);
        }

    }
}
