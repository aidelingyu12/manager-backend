package com.example.manager.Sys.Topic.service;

import com.example.manager.Sys.Topic.bean.Topic;

import java.util.List;

public interface getTopicService {
    List<Topic> getAllTopics();

    void setTopicInfo(List<String> topicInfo);
}
