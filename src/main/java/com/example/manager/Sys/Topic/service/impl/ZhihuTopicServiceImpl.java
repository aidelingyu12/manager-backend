package com.example.manager.Sys.Topic.service.impl;

import com.example.manager.Sys.Topic.cores.utils.RedisUtil;
import com.example.manager.Sys.Topic.cores.utils.topicResult;
import com.example.manager.Sys.Topic.dao.topicDao;
import com.example.manager.Sys.Topic.service.TimingCrawlZhihuService;
import com.example.manager.Sys.Topic.service.zhihuTopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ZhihuTopicServiceImpl implements zhihuTopicService {

    private Logger logger = LoggerFactory.getLogger(ZhihuTopicServiceImpl.class);

    @Autowired
    topicDao topicDao;

    @Autowired
    TimingCrawlZhihuService timingCrawlService;

    @Resource
    private RedisUtil redisUtil;

    @Override
    public topicResult getZhihuTopics()   {
        if(redisUtil.lGetListSize("zhihu") <= 0){
            logger.info("开始首次获取数据");
            List<Object> topicJsonResult = timingCrawlService.getZhihuTopicOnce();
            return topicResult.putMSGAndData(200,"获取知乎热搜成功！",topicJsonResult);
        }else{
            Long zhihuTopicSize = redisUtil.lGetListSize("zhihu");
            logger.info("开始获取知乎热搜数据");
            List<Object> resultList = redisUtil.lGet("zhihu",zhihuTopicSize-40,zhihuTopicSize-1);
            if(resultList != null){
                for(Object obj : resultList)
                    logger.info(obj.toString());
                return topicResult.putMSGAndData(200,"获取知乎热搜成功！",resultList);
            }else{
                return null;
            }
        }
    }

}
