package com.example.manager.Sys.Topic.service.impl;

import com.example.manager.Sys.Topic.bean.Topic;
import com.example.manager.Sys.Topic.cores.utils.RedisUtil;
import com.example.manager.Sys.Topic.dao.topicDao;
import com.example.manager.Sys.Topic.service.getTopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class getTopicServiceImpl implements getTopicService {

    @Autowired
    topicDao topicDao;

    @Resource
    private RedisUtil redisUtil;

    @Override
    public List<Topic> getAllTopics() {
        List<Topic> topicList = topicDao.getAllTopics();
        //将热搜列表放入热搜类型中
        for(Topic topic : topicList){
            //获取redis热搜key
            String redisKey = topic.getTopicTitle();
            //获取热搜总数
            Long kLength = redisUtil.lGetListSize(redisKey);
            //百度获取20条，知乎、微博获取50条
            if(redisKey.equals("baidu")){
                List<Object> topicRes = redisUtil.lGet(redisKey,kLength-20,kLength-1);
                //将热搜列表放入热搜类型中
                topic.setTopicInfo(topicRes);
            }else{
                List<Object> topicRes = redisUtil.lGet(redisKey,kLength-40,kLength-1);
                //将热搜列表放入热搜类型中
                topic.setTopicInfo(topicRes);
            }

        }
        return topicList;
    }

    @Override
    public void setTopicInfo(List<String> topicInfo) {
        this.topicDao.setTopicInfo(topicInfo);
    }
}
