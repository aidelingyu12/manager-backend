package com.example.manager.util;

import com.example.manager.Sys.Authority.bean.Permisson;

import java.util.HashMap;
import java.util.List;

/**
 * 结果集类,meta包含code状态码和msg信息；data包含数据集合
 */
public class Result extends HashMap<String,Object> {
    private static final long serialVersionUID = 1L;

    //在第一次引用类的任何成员时创建实例，公共语言运行库负责处理变量初始化
    private static Result instance = new Result();

    private Result() { }
    public static Result getResult()
    {
        return instance;
    }

    //返回返回meta
    public static Result putMETA(int code,String msg){
        Result MSG = new Result();
        Result META = new Result();
        MSG.put("code",code);
        MSG.put("msg",msg);
        META.put("meta",MSG);
        return META;
    }


    //返回data
    public static Result putList(List<Permisson> data){
        Result res = new Result();
        res.put("data",data);
        return res;
    }

    //返回data
    private Result putData(Result result){
        Result res = new Result();
        res.put("data",result);
        return res;
    }

    //返回数据
    public static Result putMSGAndData(int code,String msg,Object data){
        Result RES = new Result();
        Result MSG = new Result();
        Result META = new Result();
        MSG.put("code",code);
        MSG.put("msg",msg);
        RES.put("meta",MSG);
        RES.put("data",data);
        return RES;
    }

    //构造方法
    public Result put(String key,Object value){
        super.put(key,value);
        return this;
    }

}
