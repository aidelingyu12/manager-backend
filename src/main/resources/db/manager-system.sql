/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80021
Source Host           : localhost:3306
Source Database       : manager-system

Target Server Type    : MYSQL
Target Server Version : 80021
File Encoding         : 65001

Date: 2021-01-21 15:19:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for easypipe
-- ----------------------------
DROP TABLE IF EXISTS `easypipe`;
CREATE TABLE `easypipe` (
  `pipe_id` int NOT NULL AUTO_INCREMENT,
  `pipe_name` varchar(20) NOT NULL,
  PRIMARY KEY (`pipe_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of easypipe
-- ----------------------------
INSERT INTO `easypipe` VALUES ('8', 'ddff');
INSERT INTO `easypipe` VALUES ('9', 'create');
INSERT INTO `easypipe` VALUES ('10', 'ddfdf');

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `pm_id` smallint NOT NULL,
  `pm_name` varchar(20) NOT NULL,
  `pm_pid` smallint DEFAULT NULL,
  `pm_level` enum('0','1','2') DEFAULT '0' COMMENT '权限等级',
  `pm_path` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`pm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES ('101', '热搜管理', '0', '0', null);
INSERT INTO `permission` VALUES ('102', '权限中心', '0', '0', null);
INSERT INTO `permission` VALUES ('103', '日志中心', '0', '0', null);
INSERT INTO `permission` VALUES ('104', '用户管理', '102', '1', 'user');
INSERT INTO `permission` VALUES ('105', '角色管理', '102', '1', 'role');
INSERT INTO `permission` VALUES ('106', '组织管理', '102', '1', 'community');
INSERT INTO `permission` VALUES ('107', '个人信息', '102', '1', 'personalInfo');
INSERT INTO `permission` VALUES ('108', '持续集成', '0', '0', null);
INSERT INTO `permission` VALUES ('109', '热搜列表', '101', '1', 'topic');
INSERT INTO `permission` VALUES ('110', '简易流水线', '108', '1', 'easyPipe');
INSERT INTO `permission` VALUES ('111', 'pipeline流水线', '108', '1', 'pipeline');
INSERT INTO `permission` VALUES ('112', '任务模板', '108', '1', 'mission');

-- ----------------------------
-- Table structure for topic
-- ----------------------------
DROP TABLE IF EXISTS `topic`;
CREATE TABLE `topic` (
  `topic_id` int NOT NULL AUTO_INCREMENT,
  `createTime` datetime(6) DEFAULT NULL,
  `deleteTime` datetime(6) DEFAULT NULL,
  `baseUrl` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `isDeleted` int DEFAULT NULL,
  `topic_title` varchar(255) DEFAULT NULL,
  `topicInfo` varchar(255) DEFAULT NULL,
  `lastUpdateTime` datetime(6) DEFAULT NULL,
  `topicTitleCh` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3816 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of topic
-- ----------------------------
INSERT INTO `topic` VALUES ('1', null, null, 'http://top.baidu.com/?fr=mhd_card', null, 'baidu', null, null, '百度');
INSERT INTO `topic` VALUES ('2', null, null, 'https://s.weibo.com/top/summary?cate=realtimehot', null, 'weibo', null, null, '微博');
INSERT INTO `topic` VALUES ('3', null, null, 'https://www.zhihu.com/billboard', null, 'zhihu', null, null, '知乎');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_name` varchar(20) NOT NULL,
  `user_id` smallint NOT NULL AUTO_INCREMENT,
  `group_id` smallint DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `type` smallint DEFAULT NULL COMMENT '系统同步或者自定义',
  `state` smallint DEFAULT NULL COMMENT '正常或者禁用',
  `isDeleted` smallint NOT NULL DEFAULT '0' COMMENT '是否删除',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('admin', '1', null, '2548415331@qq.com', '18962861309', null, '0', '0', null, null, '123456');
INSERT INTO `user` VALUES ('user01', '2', null, null, null, null, '1', '0', null, null, '123456');
INSERT INTO `user` VALUES ('user02', '3', null, null, null, null, '0', '0', null, null, '123456');
INSERT INTO `user` VALUES ('user03', '4', null, null, null, null, '0', '0', null, null, '123456');
INSERT INTO `user` VALUES ('1', '39', '1', '1', '1', null, null, '0', null, null, '123456');
INSERT INTO `user` VALUES ('2', '40', '2', '2', '2', null, null, '0', null, null, '123456');
INSERT INTO `user` VALUES ('1', '41', '1', '', '', null, '0', '0', null, null, '');
